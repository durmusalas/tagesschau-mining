# TestPackage.jl

[![pipeline status](https://collaborating.tuhh.de/ctk3675/testpackage.jl/badges/master/pipeline.svg)](https://collaborating.tuhh.de/ctk3675/testpackage.jl/commits/master)

This package is a TestPackage to demonstrate Git in combination with a Julia package.

#### getChannelArticles — Function
```
getChannelArticles(channel::String, keywards::Vector{String}, uniqueId=false, channelsFile="taggeschau.csv", articlesFile="tags.csv")
```
Return a DataFrame contining the ID of each article published by the channel `channel` about specific topics `keywards`

`channel`: channel name

`keywards`: list of topics

`uniqueId`: if set to true duplicate articles IDs will be discarded

`channelsFile`: path to the file which contains all channels

`articlesFile`: path to the file which contains all articles

#### getGeoOrigin — Function
```
getGeoOrigin(df::DataFrame)
```
Return a DataFrame containing the geographical origin of each article in df

`df`: list of articles

### getDevelopmentOfaTag - Function
```
getDevelopmentOfaTag(str::String)
```
Return the development of a specific tag

`str`: tag

### groupbynrow - Function
```
groupbynrow(df, symbol)
```
Groups the same values in the column and counts the rows.

`df`:File as DataFrame

`symbol`: column name of df

### topTagsDf - Function
```
topTagsDf(x, df)
```
Return the first x Top Tags of df.

`x`: Int

`df`: File as DataFrame

### filterbysource -Function
```
filterbysource(y,df1,df2)
```
Return a DataFrame with the Tags that only appears on the certain source.

`y`: source

`df1`: taggeschau.csv

`df2`: tags.csv

### filternotsource - Function
```
filternotsource(y,df1,df2)
```
Return a DataFrame with the Tags that not appears on the certain source.

`y`: source

`df1`: taggeschau.csv

`df2`: tags.csv

### filterbybundesland - Function
```
filterbybundesland(quelle,df1,df2)
```
Return the Bundeländer Tags from the certain source.

`quelle`: source

`df1`: taggeschau.csv

`df2`: tags.csv

### partnerTag - Function
```
partnerTag(tag,df2)
```
Return the other Tags which are in the same article as the variable tag.

`tag`: String

`df2`: tags.csv

