# Roadmap

### Woche 1
* Daten anschauen
* Passende Fragen ausdenken

### Woche 2
* Passende Methoden schreiben
* README
* Tests schreiben
* Pluto Notebook ergänzen

### Woche 3
* Visualisierung der Daten
* README
* Tests schreiben
* Pluto Notebook ergänzen

### Woche 4
* Dokumentation der Ergebnisse fertigstellen
* Pluto Notebook fertigstellen
* Präsentation vorbereiten