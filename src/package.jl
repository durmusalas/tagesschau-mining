module MainModule

import Pkg
import Test
using Test
Pkg.add("DataFrames")
Pkg.add("CSV")
Pkg.add("Plots")
using Dates, UUIDs, DataFrames, CSV, Plots, Pkg

export groupbynrow,topTagsDf,filterbybundesland,filterbysource,filterbysource,partnerTag, getDevelopmentOfaTag, getChannelArticles, getGeoOrigin


begin

    startDate = Date("2022-03-14")

    endDate = Date("2022-05-30")
    
    df = DataFrame(CSV.File("taggeschau.csv",types=Dict(:externalId=>UUID)))
    filter!(:datetimepublished => x -> x > startDate,df)
    filter!(:datetimepublished => x -> x < endDate,df)
    tagsdf = DataFrame(CSV.File("tags.csv",types=Dict(:externalId=>UUID)))
    geotagsdf = DataFrame(CSV.File("geotags.csv",types=Dict(:externalId=>UUID)))#



    publisheddf = select(df,:datetimepublished=>(x -> Date.(x))=>:datepublished)
    #alldf = innerjoin(df, tagsdf, geotagsdf, on = :externalId)

    bundesland = ("Baden-Württemberg", "Bayern", "Berlin", "Brandenburg", "Bremen", "Hamburg", "Hessen", "Mecklenburg-Vorpommern", "Niedersachsen", "Nordrhein-Westfalen", "Rheinland-Pfalz", "Saarland", "Sachsen", "Sachsen-Anhalt", "Schleswig-Holstein", "Thüringen")
    tagsdfWithOutBundesland = filter(:tag => x -> x ∉ bundesland, tagsdf)

    dfwithDate = hcat(publisheddf, df)
    alldfNoBundesland = innerjoin(dfwithDate, tagsdfWithOutBundesland, geotagsdf, on = :externalId)

end;

"""
Function `getDevelopmentOfaTag`
Return the development of a specific tag

Use:
* getDevelopmentOfaTag(str::String)

Arguments:
* str: tag
"""
function getDevelopmentOfaTag(str::String)
    tag = alldfNoBundesland[(alldfNoBundesland.tag .== str),:]
    dateAndTag = tag[:,[:datepublished,:tag]]

    date = select(dateAndTag, :datepublished)
    gdf = groupby(date, :datepublished)
    cgdf = combine(gdf, nrow)

    plot(cgdf.datepublished, cgdf.nrow, xlabel = "Date", ylabel = "Anzahl der Artikel", label = str, ylim = (0,30))

end

"""
Function `groupbynrow`
Groups the same values in the column and counts the rows.

Use:
* groupbynrow(df, symbol)

Arguments:
* df:File as DataFrame
* symbol: column name of df
"""
function groupbynrow(df, symbol)
    dh1 = groupby(df, symbol)
    dh2 = sort!(combine(dh1, nrow),:nrow, rev=true)

    @test first(dh2).nrow >= last(dh2).nrow 
    return dh2
end

"""
Function `topTagsDf`
Return the first x Top Tags of df.

Use:
* topTagsDf(x, df)

Arguments:
* x: Int
* df: File as DataFrame
"""
function topTagsDf(x, df)
    dh1 = groupbynrow(df, :tag)
    dh2 = first(dh1, x)
    return dh2
end

"""
Function `filterbysource`
Return a DataFrame with the Tags that only appears on the certain source.

Use:
* filterbysource(y,df1,df2)

Arguments:
* y: source
* df1: taggeschau.csv
* df2: tags.csv
"""
function filterbysource(y,df1,df2)
    fdf = filter(:source => x -> x == y, df1)
    a = innerjoin(fdf,df2,on=:externalId)
    b = groupbynrow(a, :tag)
    return b
end

"""
Function `filternotsource`
Return a DataFrame with the Tags that not appears on the certain source.

Use:
* filternotsource(y,df1,df2)

Arguments:
* y: source
* df1: taggeschau.csv
* df2: tags.csv
"""
function filternotsource(y,df1,df2)
    fdf = filter(:source => x -> x != y, df1)
    a = innerjoin(fdf,df2,on=:externalId)
    b = groupbynrow(a, :tag)
    return b
end

"""
Function `filterbybundesland`
Return the Bundeländer Tags from the certain source.

Use:
* filterbybundesland(quelle,df1,df2)

Arguments:
* quelle: source
* df1: taggeschau.csv
* df2: tags.csv
"""
function filterbybundesland(quelle,df1,df2)
    bundesland = ("Baden-Württemberg", "Bayern", "Berlin", "Brandenburg", "Bremen", "Hamburg", "Hessen", "Mecklenburg-Vorpommern", "Niedersachsen", "Nordrhein-Westfalen", "Rheinland-Pfalz", "Saarland", "Sachsen", "Sachsen-Anhalt", "Schleswig-Holstein", "Thüringen")
    a = filter(:source => x -> x == quelle, df1)
    b = innerjoin(a,df2,on=:externalId)
    c = filter(:tag => x -> x in bundesland, b)
    d = groupbynrow(c, :tag)
    return d
end

"""
Function `partnerTag`
Return the other Tags which are in the same article as the variable tag.

Use:
* partnerTag(tag,df2)

Arguments:
* tag: String
* df2: tags.csv
"""
function partnerTag(tag,df2)
    dh = filter(:tag => x -> x == tag, df2)
    eh =innerjoin(dh, df2,on=:externalId,makeunique=true)
    fh = select(eh, :tag_1)
    gh = filter(:tag_1 => x -> x != tag, fh)
    #s = groupbynrow(gh,:tag_1)
    return gh
end

"""
Function `getChannelArticles`
returns a DataFrame contining the ID of each article published by the channel channel about specific topics keywards.

Use:
* getChannelArticles(channel, keywards, uniqueId, channelsFile, articlesFile)

Arguments:
* channel: channel name
* keywards: list of topics
* uniqueId: if set to true duplicate articles IDs will be discarded, default is false
* channelsFile: path to the file which contains all channels, default is "taggeschau.csv"
* articlesFile: path to the file which contains all articles, default is "tags.csv"
"""
function getChannelArticles(channel::String, keywards::Vector{String}, uniqueId::Bool=false, channelsFile::String="taggeschau.csv", articlesFile::String="tags.csv")::DataFrame
    
    channelsDF  = filter(row -> row.source == channel, DataFrame(CSV.File(channelsFile)))
    articlesDF  = DataFrame(CSV.File(articlesFile))
    articlesIds = combine(channelsDF, :externalId)

    df = filter(row -> size(filter(row2 -> row2.externalId == row.externalId, articlesIds))[1] > 0 && row.tag in keywards, articlesDF)

    # Check if the number of selected articles is smaller than or equal to the number of all articles
    @test size(df)[1] >= 0 && size(df)[1] <= size(articlesDF)[1]

    if uniqueId
        return unique(df, :externalId)
    end
    return df

end

"""
Function `getGeoOrigin`
returns a DataFrame containing the geographical origin of each article in df.

Use:
* getGeoOrigin(df)

Arguments:
* df: list of articles
"""
function getGeoOrigin(df::DataFrame)::DataFrame

    originsDF = filter(row -> row.tag != "(Keine Auswahl)" && size(filter(row2 -> row2.externalId == row.externalId, df))[1] > 0, DataFrame(CSV.File("geotags.csv")))

    # Check if the number of origins is smaller than or equal to the number of articles
    @test size(originsDF)[1] >= 0 && size(originsDF)[1] <= size(df)[1]

    return originsDF

end

end